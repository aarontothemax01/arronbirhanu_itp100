from collections import defaultdict

with open('input_data1.txt', 'r') as fp:
    line = fp.readlines()

def make_anagram_dict(line):
    d = defaultdict(list)  # argument is the default constructor for value

    for word in line:
        word = word.lower()  # call lower() only once
        key = ''.join(sorted(word))
        d[key].append(word)  # now d[key] is always list

    return d  # just return the mapping to process it later

if __name__ == '__main__':
    d = make_anagram_dict(line)

    for words in d.values():
        if len(words) > 1:  # several anagrams in this group
            print('Anagrams: {}'.format(', '.join(words)))
