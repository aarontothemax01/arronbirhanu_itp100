
##def silly_sentence(job, adjective):
##  """   
##  silly_sentence('lumberjack', 'okay')
##      "I'm a lumberjack and I'm okay."
##      >>> silly_sentence('student', 'real smart')
##      "I'm a student and I'm real smart."
##      >>> silly_sentence('truck driver', 'sleepy')
##      "I'm a truck driver and I'm sleepy."
##return "I'm a " + job + " and I'm " + adjective + "."      
##    """
##
##
##if __name__ == '__main__':
##     import doctest
##     doctest.testmod()
def silly_sentence(job, adjective):
    """
silly_sentence('lumberjack', 'okay')
      "I'm a lumberjack and I'm okay."
      >>> silly_sentence('student', 'real smart')
      "I'm a student and I'm real smart."
      >>> silly_sentence('truck driver', 'sleepy')
      "I'm a truck driver and I'm sleepy."
    """
    return "I'm a " + job + " and I'm " + adjective + "."



if __name__ == '__main__':
    import doctest
    doctest.testmod()
