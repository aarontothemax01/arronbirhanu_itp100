def is_palindrome(s):   
    """
      >>> is_palindrome('abba')
      True
      >>> is_palindrome('abab')
      False
      >>> is_palindrome('tenet')
      True
      >>> is_palindrome('banana')
      False
      >>> is_palindrome('straw warts')
      True
    """
    if s == '':
        return True

    if s[0] !=  s[-1]:
        return False
    
    return is_palindrome(s[1:-1])


if __name__ == '__main__':
    import doctest
    doctest.testmod()
