def main(A, B, C, T):
    global counter
    counter = 0
    list_a = [A, 0]
    list_b = [0, B]
    c = C
    t = T

    converge = list_a[0] / list_b[1]

    def move_c(from_list, to_list): 
        x = from_list[0]
        y = from_list[1]
        a = to_list[0]
        b = to_list[1]

        total = x + y

        from_list[0] -= (c * x) / total
        from_list[1] -= (c * y) / total

        to_list[0] += (c * x) / total
        to_list[1] += (c * y) / total

    def iterate():
        move_c(list_a, list_b)
        move_c(list_b, list_a)

        global counter
        counter += 1
        
        global value_a
        global value_b
        value_a = list_a[0] / list_a[1]
        value_b = list_b[0] / list_b[1]

    def test_for_convergence():
        global value_a
        global value_b
        if abs(value_a - converge) < t and abs(value_b - converge) < t:
            return True
        return False

    iterate()
    while not test_for_convergence():
        iterate()
        
    print(counter)

mode = input('Do you want to read from the test data file? (Y/n) ')

if mode.lower() == 'y' or mode == '' or mode.lower() == 'yes':
    with open('test_data.dat', 'r') as f:
        for line in f:
            if line.startswith('#'):
                continue
            line = line.split()
            line = [float(x) for x in line]
            if len(line) != 4:
                continue
            else:
                A, B, C, T = line
                main(A, B, C, T)
elif mode.lower() == 'n' or mode.lower() == 'no':
    print('Then what values should I use?')
    values = input('')
    values = values.split()
    values = [float(x) for x in values]
    A, B, C, T = values
    main(A, B, C, T)
else:
    print('Please enter a valid mode. Enter defaults to Y.')

