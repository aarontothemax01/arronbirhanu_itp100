1. Dr. Chuck calls looping the 4th basic pattern of computer programming. What are the other three patterns?
Store & Reuse, Sequential, and Conditional

2. Describe the program Chr. Chuck uses to introduce the while loop. What does it do?
He initializes an int called n setting it equal to 5. He then sets his while loop to run until n is not greater than 0 (when n=0). Within the while loop he has n subtract one from its value (ex: 5-1 for the first loop) until the while loop proves true (n>0). Meanwhile he prints every value of n for each loop it goes through (5,4,3,2,1). When n=0 the function prints 'blast off.'

3. What is another word for looping introduced in the video?
Dr. Chuck also calls looping iteration

4. Dr. Chuck shows us a broken example of a loop. What is this kind of loop called? What is wrong with it?
Infinite loops run forever and sometimes crash the processor (depends on the magnitude and variety of the infinite loop). It never has a break within the loop which means not a single command is responsible for ending it.

5. What Python statement (a new keyword for us, by the way) can be used to exit a loop?
Break; My favorite actually

6. What is the other loop control statement introduced in the first video? What does it do?
Contrary to the break control statement, the Continue statement orders the loop to repeat itself.

7. Dr. Chuck ends the first video by saying that a while is what type of loop? Why are they called that? What is the type of loop he says will be discussed next?
They are a conditional loop which means they run under certain terms. The next loop he was going to talk about was for loops.

8. Which new Python loop is introduced in the second video? How does it work?
The For loop, also known as a definite loop, executes repetition according to the value set of things (characters, strings, int values). This loop will run a finite amount of times depending on the limiting factors.

9. The third video introduces what Dr. Chuck calls loop idioms. What does he mean by that term? Which examples of loop idioms does he introduce in this and the fourth video?
Even though these examples are simple, the patterns apply to all kinds of loops and loop idioms examine the boundaries and conditionality of loops.