1. Describe the characteristics of a collection.
A single package able to carry a variety of values. In the single package, there are multiple departments where each value of the collection (or package) is stored and accessible.

2. Using the video for inspiration, make a “high level” comparison of lists and dictionaries. How should you think about each of them?
A list is an ordered sequence of objects, whereas dictionaries are unordered sets. However, the main difference is that items in dictionaries are accessed via keys and not via their position. Any key of the dictionary is associated (or mapped) to a value. The values of a dictionary can be any type of Python data.

3. What is the synonym Dr. Chuck tells us for dictionaries, that he presents in the slide showing a similar feature in other programming languages?
Associative arrays - Perl/PHP
Properties - or Map or HashMap - Java
Property Bag - C# / .Net
Dictionaries are Python's most powerful data collection
Dictionaries allow us to do fast database-like operations in Python

4. Show a few examples of dictionary literals being assigned to variables.
>>> purse = dict()
>>> purse['money'] = 12
>>> purse['candy'] = 3
>>> purse['tissues'] = 75
>>> print(purse)
{'money': 12, 'tissues': 75, 'candy': 3}
>>> print(purse['candy'])
3
>>> purse['candy'] = purse['candy'] + 2
>>> print(purse)
{'money': 12, 'tissues': 75, 'candy': 5}

5. Describe the application in the second video which Dr. Chuck says is one of the most common uses of dictionaries.
One common use of dictionaries is counting how often we “see” something.
>>> ccc = dict()
>>> ccc['csev'] = 1
>>> ccc['cwen'] = 1
>>> print(ccc)
{'csev': 1, 'cwen': 1}
>>> ccc['cwen'] = ccc['cwen'] + 1
>>> print(ccc)
{'csev': 1, 'cwen': 2}

6. (As Shown Above)

7. What is the dictionary method that makes this common operation shorter (from 4 lines to 1)? Describe how to use it.
The general pattern to count the words in a line of text is to split the line into words, then loop through the words and use a dictionary to track the count of each word independently.

8. Describe the application that Dr. Chuck leads into in the third video and codes in front of us in the fourth video.
Retrieving lists of keys and values - You can get a list of keys, values, or items (both) from a dictionary
>>> jjj = { 'chuck' : 1 , 'fred' : 42, 'jan': 100}
>>> print(list(jjj))
['jan', 'chuck', 'fred']
>>> print(jjj.keys())
['jan', 'chuck', 'fred']
>>> print(jjj.values())
[100, 1, 42]
>>> print(jjj.items())
[('jan', 100), ('chuck', 1), ('fred', 42)]
>>> 
