
1.In the first video of this lesson, Dr. Chuck discusses two very important concepts: algorithms and data structures. How does he define these two concepts? Which one does he say we have been focusing on until now?

Algorithm: A set of rules or steps used to solve a problem.
Data Structure: A particular way of organizing data in a computer.
He says we've been mostly focused on algorithmic programming.


2.
-Describe in your own words what a collection is.

Containers that are used to store collections of data, for example, list, dict, set, tuple etc. These are built-in collections. Several modules have been developed that provide additional data structures to store collections of data. One such module is the Python collections module.

3.
-Dr. Chuck makes a very important point in the slide labeled Lists and definite loops - best pals about Python and variable names that he has made before, but which bares repeating. What is that point?

Loops hold repetition. When constructing a loop, the variables initialized are meant to be controllers of the loop and what values are cycled through every iteration. Dr. Chuck uses the friend vs friends as examples to express plural and singular terms are insignificant within the Python language but might be a quite difficult for beginning developers to catch on.

4.
-How do we access individual items in a list? What does mutable mean? Provide examples of mutable and immutable data types in Python.

Python has a great built-in list type named "list". List literals are written within square brackets [ ]. Lists work similarly to strings -- use the len() function and square brackets [ ] to access data, with the first element at index 0. Simple put, a mutable object can be changed after it is created, and an immutable object can't. Objects of built-in types like (int, float, bool, str, tuple, unicode) are immutable. Objects of built-in types like (list, set, dict) are mutable. Custom classes are generally mutable.

5. Describe several....
-list operations presented in the lecture:
list.append(x)
Add an item to the end of the list. Equivalent to a[len(a):] = [x].

list.extend(iterable)
Extend the list by appending all the items from the iterable. Equivalent to a[len(a):] = iterable.

list.insert(i, x)
Insert an item at a given position. The first argument is the index of the element before which to insert, so a.insert(0, x) inserts at the front of the list, and a.insert(len(a), x) is equivalent to a.append(x).

list.remove(x)
Remove the first item from the list whose value is equal to x. It raises a ValueError if there is no such item.

list.pop([i])

-useful functions that take lists as arguments presented in the lecture:

Concatenating Lists; Creating new lists by adding two original lists together

-list methods presented in the lecture:
>>> x = list()
>>> type(x)
<type 'list'>
>>> dir(x)
['append', 'count', 'extend', 'index', 'insert', 'pop', 'remove', 'reverse', 'sort']
>>> 

6.
-The third video describes several methods that allow lists and strings to interoperate in very useful ways. Describe these.
The principal built-in types are numerics, sequences, mappings, classes, instances and exceptions.

Some collection classes are mutable. The methods that add, subtract, or rearrange their members in place, and don’t return a specific item, never return the collection instance itself but None.

Some operations are supported by several object types; in particular, practically all objects can be compared for equality, tested for truth value, and converted to a string (with the repr() function or the slightly different str() function). The latter function is implicitly used when an object is written by the print() function.


-What is a guardian pattern? Use at least one specific example in describing this important concept.

The first part of the expression avoids (guards) a possible error in the second part of the expression. If you remove that first part, like in the example you show, then you would be trying to access the first element of line (line[0]), when there is none elements in line.