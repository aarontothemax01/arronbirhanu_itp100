1.What is an application protocol? List examples of specific application protocols listed in the lecture. Can you think of one besides HTTP that we have been using in our class regularly since the beginning of the year?

Answers the question of what does the user want to do with the socket and what problem does the user intend on solving.

Some examples of protocols include mail and world wide web

2.Name the three parts of a URL. What does each part specify?

The 3 parts are the protocol, host, and document. The protocal is a protocal, like https. The host is a host to connect to, like www.py4e.com, and the document is what specificly in the host to retrieve, like lessons/network#

3.What is the request/response cycle? What example does Dr. Chuck use to illustrate it and describe how it works?

When Dr. Chuck clicks the link, a request is sent to the web server to connect to port 80. Then some magic happens and then you get back some html, the computer renders it, yay!

4.What is the IETF? What is an RFC?

The IETF is the orginization who creates the internet protocal standards. These standards are called RFCs or Request for Comments. Dr. Chuck says the request sent by links to connect to another web server is specified by an internet standard.

5.In the video titled Worked Example: Sockets, Dr. Chuck tells us where to download a large collection of sameple programs he has available associated with the course. Where do we find these examples?

We go to the materials tab in python for everybody book and click the second link to download the examples Dr. Chuck gave us.

6.Try the telnet example that Dr. Chuck shows us in the Worked Example: Sockets video. Can you retrieve the document using telnet? (NOTE: examples in the previous videos no longer work. I suspect this is because HTTP/1.0 is no longer supported by the webserver).

For some reason, the telnet command doesn't exist for me so I wasn't able to do this.