1. What is ASCII? What are its limitations and why do we need to move beyond it?

ASCII is the latin character set used by us on a day to day basis, however, it ONLY contains the latin character set. No accents, no Japanese or French stuff, just the latin.

2. Which function in Python will give you the numeric value of a character (and thus its order in the list of characters)?

The 'ord()' function gives us the ASCII value of any latin character on the keyb
oard. I saw someone in class use this during the lesson on strings I think.

3. Dr. Chuck says we move from a simple character set to a super complex character set. What is this super complex character set called?

Unicode. The super complex character set is called unicode.

4. Describe bytes in Python 3 as presented in the video. Do a little web searching to find out more about Python bytes. Share something interesting that you find.

A byte is a type of binary storage containing 8 digits, either a one or zero in each. This means a byte can be equal to one of 255 different values. This means you can encode information from the ASCII character set into data in the form of bytes that can be sent across the internet and decoded by another computer.

5. Break down the process of using .encode() and .decode methods on data we send over a network connection.

When we send data over the internet, the first computer uses the encode metheod to encode the data into a byte format so you can send it in UTF-8, which is the easiest to send when compared to UTF 32 and 16. You can then use the decode function from the other computer to go from the bytes format to the original format.
